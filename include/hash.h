#ifndef HASH_H
#define HASH_H
#include "tree.h"
/*
** The hash table is fairly straightforward. It's based on a
** table that has a dynamically allocated number of elements.
** The hash function is designed to work well with strings.
*/
typedef struct _hash_table_t_ hash_table_t;
typedef struct treenode tree;

typedef struct _list_t_
{
  char* string;                 /* The actual string value */
  struct _hash_table_t_ *scope; /* The scope value of the ID */
  int type;                     /* The type (int/char/void) of the ID */
  int num_params;               /* number of params */
  int arr;                      /* 1 means it has declared size, 2 means no */
  int arr_size;                 /* size of array, if any */
  int *params;                  /* array of parameters */
  int allocated;                /* assigned to a register? */
  int reg;                      /* register present in */
  int offset;                   /* integer offset for locally spilled variables */
  struct _list_t_* next;        /* In case of hash collisions, next possible match */
} list_t;

struct _hash_table_t_
{
  int size;                     /* size of table */
  list_t** table;               /* table elements (dynamic array) */
  hash_table_t *child;          /* pointer to first child table */
  hash_table_t *next;           /* the next table on the same level */
};

/*
** Constructor. Requires an integer size value for the number of
** potential entries, or hash values. Regardless of the size you
** pick, the hash table will grow to accommodate any number of
** of strings; it's just a matter of how often the hashes collide.
** Powers of two work fairly well, so I usually use 128.
*/
hash_table_t* create_symbol_table(int size);

/* Destructor */
void free_table(hash_table_t* hashtable);

/*
** Populator. Given a table to lookup in, and a string value, this
** function adds a string to the table. A return value of 1 indicates
** an error, a value of 2 indicates the string in question was already
** in the table (read: previously declared), and 0 indicates successful
** addition of the string into the table.
*/
int add_identifier(hash_table_t *symbol_table, char *string);

/*
 * Adding a function requires handling (potentially) a parameter list.
 * This is why we have a separate function. The list of parameters is
 * stored as an integer array (KWD_INT, KWD_CHAR or KWD_STRING for values)
 */
int add_func_identifier(hash_table_t *symbol_table, tree *type, char *string, tree *params);

/*
** Lookup. Given a table to lookup in, and a string value, returns the
** list_t* to the hash table entry or NULL if the string was not found,
** as would be the case when a variable is used without declaration.
*/
list_t *lookup_identifier(hash_table_t *symbol_table, char *string);

/*
 * When looking up a new function declaration, we need to examine the entire signature,
 * which consists of the parameter list and name.
 */
list_t *lookup_func_identifier(hash_table_t *symbol_table, char *string, tree *params);

/* internal hash functions */
int hash(hash_table_t* symbol_table, char* string);
void dump_hash_table(hash_table_t* hashtable);
#endif
