#!/usr/bin/python

from subprocess import call, check_call, check_output, CalledProcessError
import os, sys

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

tests = 1
errors = 0
FNULL = open(os.devnull, 'w')
report = "index.html"

def setupHTML(f):
    f.write("""\
            <!DOCTYPE html>
            <html lang="en-US">
            <head>
            <title>SSCC Test Report</title>
            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body>
            <div class="w3-padding w3-white notranslate">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Test Number</th>
                        <th>Test Run</th>
                        <th>Result</th>
                    </tr>
                </thead>
                <tbody>
            """)

def teardownHTML(f):
    f.write("""\
                </tbody>
            </table>
            </div>
            </body>
            </html>
            """)

try:
    os.path.exists(report)
except:
    os.makedirs(report)

f = open("index.html", 'w')
setupHTML(f)

for root, dirs, files in os.walk("tests"):
    for file in files:
        if file.endswith(".pass"):
            print("Test #{0:3}: {1:45} ".format(tests, os.path.join(root, file))),
            f.write("<tr><td>{0:3}</td><td>{1:45}</td> ".format(tests, os.path.join(root, file)))
            fd = open(os.path.join(root, file), 'r')
            try:
                check_call(["./mcc",], stdout=FNULL, stderr=FNULL, stdin=fd)
                print("OK")
                f.write("<td class=success>OK</td></tr>")
            except CalledProcessError as e:
                print("{}NOT OK{}".format(bcolors.HEADER, bcolors.ENDC))
                f.write("<td class=danger>NOT OK</td></tr>")
                errors = errors + 1
            tests = tests + 1
            fd.close()
        if file.endswith(".fail"):
            print("Test #{0:3}: {1:45} ".format(tests, os.path.join(root, file))),
            f.write("<tr><td>{0:3}</td><td>{1:45}</td> ".format(tests, os.path.join(root, file)))
            fd = open(os.path.join(root, file), 'r')
            try:
                check_call(["./mcc",], stdout=FNULL, stderr=FNULL, stdin=fd)
                print("{}NOT OK{}".format(bcolors.HEADER, bcolors.ENDC))
                f.write("<td class=danger>NOT OK</td></tr>")
                errors = errors + 1
            except CalledProcessError:
                print("OK")
                f.write("<td class=success>OK</td></tr>")
            tests = tests + 1
            fd.close()
teardownHTML(f)
f.close()
tests = tests - 1
print("========================================================================")
print("======================= Pass/Fail Summary ==============================")
print("{} tests run, {} failures".format(tests, errors))

