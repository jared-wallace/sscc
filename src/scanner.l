%{
#include<stdio.h>
#include "hash.h"
#include "parser.tab.h"

void yyerror(char *);
int yycol = 1;
int intconst;
extern YYSTYPE yylval;
extern hash_table_t *global_scope;
char error_msg[500];
extern unsigned int errors;

void multi_comments();
void increment();
int good_character();
void identifier();
void bad_string();
void bad_character(int type);
void bad_comment();
int good_string();

%}
any ({notnl}|{nl})
digit [0-9]
number {digit}+
backsl "\\"
nl "\n"
notnl .
tab \t
quote \"
tick [\']
ccchar [^\'\n\\]
whitespace [ \t\r\f]
letter [a-zA-Z]
alpha [a-zA-Z0-9]
esc ({backsl}{any})
str [^\"\n\\]
yylineno = 1

%%

"//".*                          { /* ignore comments */ }
"/""*"([^*]|"*"*[^*/])*"*"+"/"  { multi_comments();/*ignore comments */ }
"/""*"([^*]|"*"*[^*/])*"*"*     { increment(); bad_comment(); yyterminate(); }
"int"                           { increment(); return KWD_INT; }
"char"                          { increment(); return KWD_CHAR; }
"if"                            { increment(); return KWD_IF; }
"else"                          { increment(); return KWD_ELSE; }
"while"                         { increment(); return KWD_WHILE; }
"return"                        { increment(); return KWD_RETURN; }
"void"                          { increment(); return KWD_VOID; }
"string"                        { increment(); return KWD_STRING; }
{nl}                            { yycol = 1; yylineno++; }
{whitespace}                    { increment(); }
{letter}{alpha}*                { increment(); identifier(); return ID; }
[0-9]|[1-9]{digit}*             { increment(); yylval.int_val = atoi(yytext); return INTCONST; }
{tick}({ccchar}|{esc}){tick}    { increment(); if ( good_character() ) yyerror(error_msg); return CHARCONST; }
{tick}({ccchar}|{esc}){nl}      { increment(); bad_character(1); yyterminate(); }
{tick}({ccchar}|{esc})*         { increment(); bad_character(2); yyterminate(); }
\"({esc}|{str})*\"              { increment(); if ( good_string() ) yyerror(error_msg); return STRCONST; }
\"({esc}|{str})*{nl}            { increment(); bad_string(); yyterminate(); }
"+"                             { increment(); return OPER_ADD; }
"-"                             { increment(); return OPER_SUB; }
"*"                             { increment(); return OPER_MUL; }
"/"                             { increment(); return OPER_DIV; }
"<"                             { increment(); return OPER_LT; }
">"                             { increment(); return OPER_GT; }
"<="                            { increment(); return OPER_LTE; }
">="                            { increment(); return OPER_GTE; }
"="                             { increment(); return OPER_ASGN; }
"=="                            { increment(); return OPER_EQ; }
"!="                            { increment(); return OPER_NEQ; }
"["                             { increment(); return LSQ_BRKT; }
"]"                             { increment(); return RSQ_BRKT; }
"{"                             { increment(); return LCRLY_BRKT; }
"}"                             { increment(); return RCRLY_BRKT; }
"("                             { increment(); return LPAREN; }
")"                             { increment(); return RPAREN; }
","                             { increment(); return COMMA; }
";"                             { increment(); return SEMICLN; }
.                               { return ILLEGAL_TOK; }
%%
void increment()
{
        yycol = yycol + yyleng;
}

int good_character()
{
    if (yytext[1] == '\\')
    {
        switch (yytext[2])
        {
            case 'n':
                yytext[1] = '\n';
                break;
            case 't':
                yytext[1] = '\t';
                break;
            case '\"':
                yytext[1] = '\"';
                break;
            case '\\':
                yytext[1] = '\\';
                break;
            default:
                sprintf(error_msg, "%s (%d:%d)\n", "Illegal character literal at ", yylineno, yycol-yyleng);
                return 1;
        }
    }
    yylval.int_val = yytext[1];
    return 0;
}

int good_string()
{
    char *temp;
    temp = calloc(strlen(yytext)-1, sizeof(char));
    int i;
    for (i = 1; yytext[i] != '\0'; i++)
    {
        if (yytext[i] != '\\')
        {
            temp[i-1]=yytext[i];
        }
        else
        {
            switch (yytext[i+1])
            {
                case 'n':
                    temp[i-1] = '\n';
                    yytext++;
                    break;
                case 't':
                    temp[i-1] = '\t';
                    yytext++;
                    yycol = yycol + 8;
                    break;
                case '\\':
                    temp[i-1] = '\\';
                    yytext++;
                    break;
                case '\"':
                    temp[i-1] = '\"';
                    yytext++;
                    break;
                default:
                    sprintf(error_msg, "Illegal escape at (%d:%d)\n", yylineno, yycol-yyleng);
                    return 1;

            }
        }
    }

    temp[i-2] = '\0';
    yylval.identifier = malloc(strlen(temp) + 1);
    strcpy(yylval.identifier, temp);
    return 0;
}

void identifier()
{
    yylval.identifier = malloc(strlen(yytext) + 1);
    strcpy(yylval.identifier, yytext);
}

void bad_string()
{
    sprintf(error_msg, "Unterminated string at (%d:%d)\n", yylineno, yycol - yyleng);
    yyerror(error_msg);
    return;
}

void bad_character(int type)
{
    if (type == 1)
    {
        sprintf(error_msg, "Unterminated character literaL at (%d:%d)\n", yylineno, yycol - yyleng);
        yyerror(error_msg);
    }
    else
    {
        sprintf(error_msg, "Illegal character literal at (%d:%d)\n", yylineno, yycol - yyleng);
        yyerror(error_msg);
    }
}

void bad_comment()
{
    sprintf(error_msg, "Unterminated comment at (%d:%d)\n", yylineno, yycol - yyleng);
    yyerror(error_msg);
}


void multi_comments()
{
    int i;
    for (i = 0; i < yyleng; i++)
        if (yytext[i] == '\n')
            yylineno++;
}
