SSCC
======
Super Simple C Compiler

Compilation Instructions
------
1. For a debug build, use `make`, for a release build use `make DEBUG=0`. To remove all object files
and executables, use `make clean`.

2. Pre-requisite dependencies for a RHEL/CENTOS/Fedora system are as follows

    * flex
    * flex-devel
    * gcc
    * bison
    * bison-devel

3. Required files for successful compilation (If you cloned the repo, you should already have these)

    * hash.c
    * hash.h
    * tree.h
    * tree.c
    * driver.c
    * scanner.l
    * parser.y
    * codegen.c
    * codegen.h

4. To run the tests, use `make test`
5. To dump the symbol tables, pass the "-s" option to mcc on the command line.
6. To print the AST, pass the "-t" option to mcc on the command line.
7. To print debugging statements, pass the "-d" option to mcc on the command line.
8. To print the code to standard out as well as to the file, pass the "-o" option to mcc on the command line.
9. Finally, to compile a program, pass it into mcc on standard input.

Special Considerations
------
Mcc generates assembly code by first constructing a linked list of assembly "nodes", and then
printing the entire list.

The resulting asm file (called "out.s") must be run on the Mars simulator.

Placing a return statement in main will probably crash Mars (it expects a syscall 10, which is automatically provided by mcc).

The register allocater is brutally stupid and inefficient. Mcc loads the first 7 local variables
into temp registers, and any remaining local variables stay on the stack, loaded and immediately stored back when needed.
It uses three registers as true temps, (t6, t7,t8), and frees them up as soon as possible after they have served their purpose.
The only benefit to this arrangment is a one pass code generation. Drawbacks include the very real possibility of a memory leak,
where a temp register gets "frozen", causing overflow. This condition will cause a message ("register overflow") to be printed to stdout.

If using gdb to run mcc, you can call "print\_asm" at anytime to print the current assembly node linked list.

*NOTE: You must select "Initialize program counter to global main if defined" under Mars's Settings!*

Calling Convention:

1. Since we are only accepting a maximum of one parameter, I use a standard 48 byte stack frame for
all functions, subject to adjustment for local variables in excess of 7. (Locals pushed onto the stack go
from bottom up)

2. Stack frame (m local variables in excess of 7):

|address|value|comments|
|-------|----|---------------------|
| plus 44 + m| | local variable 0    |
|         |    |                     |
| plus 48 |    | local variable m-1  |
| plus 44 | ra | return address      |
| plus 40 | s6 | saved register 6    |
| plus 36 | s5 | saved register 5    |
| plus 32 | s4 | saved register 4    |
| plus 28 | s3 | saved register 3    |
| plus 24 | s2 | saved register 2    |
| plus 20 | s1 | saved register 1    |
| plus 16 | s0 | saved register 0    |
| plus 12 | a3 | empty space for arg |
| plus 8  | a2 | empty space for arg |
| plus 4  | a1 | empty space for arg |
| $sp     | a0 | empty space for arg |

3. Even though a1-a3 are never used, we'd just end up having to pad it if we didn't have them.

4. temp registers (t0-t9) are caller saved, s registers (s0-s6) are callee saved. The return address register is also callee saved.

Known Bugs
------
None known at this time. Bugs can be reported to jared@jared-wallace.com.