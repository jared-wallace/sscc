#include <string.h>
#include "tree.h"
#include "hash.h"
#include "parser.tab.h"

extern tree *parseTree;
extern hash_table_t *global_var_scope;

/*
** Having string values for AST node types makes tree output more readable.
** These must match with the nodeTypes in tree.h
*/
char *nodeNames[] =
{
    "PROGRAM",
    "DECLLIST",
    "DECL",
    "VARDECL",
    "TYPESPECIFIER",
    "FUNDECL",
    "FORMALDECLLIST",
    "FORMALDECL",
    "FUNBODY",
    "LOCALDECLLIST",
    "STATEMENTLIST",
    "STATEMENT",
    "COMPOUNDSTMT",
    "ASSIGNSTMT",
    "CONDSTMT",
    "LOOPSTMT",
    "RETURNSTMT",
    "VAR",
    "EXPRESSION",
    "RELOP",
    "ADDEXPR",
    "ADDOP",
    "TERM",
    "MULOP",
    "FACTOR",
    "FUNCCALLEXPR",
    "ARGLIST",
    "OUTPUT"
};


tree *makeTree (int kind)
{
  tree *this = (tree *) malloc ( sizeof(struct treenode) );
  this->nodeKind = kind;
  this->val = 0;
  this->id = NULL;
  this->child = NULL;
  this->parent = NULL;
  this->next = NULL;
  return this;
}

tree *makeTreeWithIntVal (int kind, int val)
{
    tree *this = (tree *) malloc ( sizeof(struct treenode) );
    this->nodeKind = kind;
    this->child = NULL;
    this->parent = NULL;
    this->next = NULL;
    this->val = val;
    this->id = NULL;
    return this;
}

tree *makeTreeWithIDVal (int kind, char *val)
{
    tree *this = (tree *) malloc ( sizeof(struct treenode) );
    this->nodeKind = kind;
    this->child = NULL;
    this->parent = NULL;
    this->next = NULL;
    this->val = 0;
    this->str_val = malloc(strlen(val));
    strcpy(this->str_val, val);
    return this;
}

tree *copyTree(int kind, tree *target)
{
    tree *this = (tree *) malloc ( sizeof(struct treenode) );
    this->nodeKind = kind;
    this->val = target->val;
    this->str_val = target->str_val;
    this->scope_table = target->scope_table;
    this->id = target->id;
    this->arr_size = target->arr_size;
    this->arr = target->arr;
    this->parent = target->parent;
    this->next = target->next;
    this->child = target->child;
}



void addChild (tree *parent, tree *child)
{
    if (parent->child == NULL)
    {
        /* first child to be added */
        parent->child = child;
        child->parent = parent;
    }
    else
    {
        tree *curr;
        curr = parent->child;       /* first child */
        while (curr->next != NULL)
            curr = curr->next;      /* find end of child list */
        curr->next = child;
        child->parent = parent;
    }
}

int get_table(hash_table_t *hashtable)
{
    return hashtable - global_var_scope;
}

void printAst (tree *node, int nestLevel)
{
    int i, j;
    printf("%s\t", nodeNames[node->nodeKind]);
    switch (node->nodeKind)
    {
        case FACTOR:
            switch (node->type)
            {
                case KWD_INT:
                    printf("(int) %d ", node->val);
                    break;
                case KWD_CHAR:
                    printf("(char) %c ", node->val);
                    break;
                case KWD_STRING:
                    printf("(string) \"%s\" ", node->str_val);
                    break;
            }
            break;
        case ADDEXPR:
        case TERM:
            switch (node->type)
            {
                case KWD_INT:
                    printf("(int) ");
                    break;
                case KWD_CHAR:
                    printf("(char) ");
                    break;
                case KWD_STRING:
                    printf("(string) ");
                    break;
            }
            break;
        case FUNDECL:
            if (node->id != NULL)
            {
                /* print type */
                switch (node->id->type)
                {
                    case KWD_INT:
                        printf("int ");
                        break;
                    case KWD_CHAR:
                        printf("char ");
                        break;
                    case KWD_VOID:
                        printf("void ");
                        break;
                }
                /* print name */
                printf("%s(", node->id->string);
            }
            int num;
            /* print parameter list, if any */
            if ( (num = node->id->num_params) != 0)
            {
                int i;
                for (i = 0; i < num-1; i++)
                {
                    switch (node->id->params[i])
                    {
                        case KWD_INT:
                            printf("int,");
                            break;
                        case KWD_CHAR:
                            printf("char,");
                            break;
                        case KWD_STRING:
                            printf("char*,");
                            break;
                    }
                }
                switch (node->id->params[i])
                {
                    case KWD_INT:
                        printf("int");
                        break;
                    case KWD_CHAR:
                        printf("char");
                        break;
                    case KWD_STRING:
                        printf("char*");
                        break;
                }
            }
            printf(")");
            /* 
             * Print function scope table address. This allows
             * easy verification of the function's variable scoping.
             */
            printf(" (Scope table: %d)", get_table(node->scope_table));
            break;
        case VARDECL:
        case FORMALDECL:
        case TYPESPECIFIER:
            if (node->id != NULL)
            {
                /* print scope */
                if (node->id->scope == global_var_scope)
                    printf("global ");
                else
                {
                    printf("local to ");
                    printf("%d ", get_table(node->id->scope));
                }
                /* print type */
                switch (node->id->type)
                {
                    case KWD_INT:
                        printf("int ");
                        break;
                    case KWD_CHAR:
                        printf("char ");
                        break;
                    case KWD_VOID:
                        printf("void ");
                        break;
                }
                /* print name */
                printf("%s", node->id->string);
            }
            /* print brackets and/or size if appropriate */
            if (node->arr == 1)
                printf("[%d]", node->arr_size);
            if (node->arr == 2)
                printf("[]");
            break;
        case RELOP:
        case ADDOP:
        case MULOP:
            switch (node->val)
            {
                case KWD_INT:
                    printf("int ");
                    break;
                case KWD_CHAR:
                    printf("char ");
                    break;
                case KWD_VOID:
                    printf("void ");
                    break;
                case OPER_LT:
                    printf("<");
                    break;
                case OPER_LTE:
                    printf("<=");
                    break;
                case OPER_GT:
                    printf(">");
                    break;
                case OPER_GTE:
                    printf(">=");
                    break;
                case OPER_EQ:
                    printf("==");
                    break;
                case OPER_NEQ:
                    printf("!=");
                    break;
                case OPER_ADD:
                    printf("+");
                    break;
                case OPER_SUB:
                    printf("-");
                    break;
                case OPER_MUL:
                    printf("*");
                    break;
                case OPER_DIV:
                    printf("\\");
                    break;
            }
            break;
        case RETURNSTMT:
            if (node->type == KWD_INT)
                printf("%d", node->val);
        case DECL:
        case FORMALDECLLIST:
        case FUNBODY:
        case LOCALDECLLIST:
        case STATEMENTLIST:
        case STATEMENT:
        case COMPOUNDSTMT:
        case ASSIGNSTMT:
        case CONDSTMT:
        case LOOPSTMT:
        case VAR:
            if (node->id != NULL)
            {
                if (node->id->scope == global_var_scope)
                    printf("global ");
                else
                {
                    printf("local to ");
                    printf("%d ", get_table(node->id->scope));
                }
                printf("\t%s", node->id->string);
            }
            if (node->arr != 0)
                printf("[]", node->arr_size);
            break;
        case FUNCCALLEXPR:
            if (node->id != NULL)
                printf("\t%s()", node->id->string);
            break;
        case EXPRESSION:
        case ARGLIST:
            break;
    }
    printf("\n");

    tree *curr;
    curr = node;

    /* if we have a child, recurse */
    if (curr->child != NULL)
    {
        curr = curr->child;
        for (j = 0; j < (nestLevel); j++)
            printf ("| ");
        if (nestLevel >= 0)
            printf ("+ ");
        printAst(curr, nestLevel + 1);
    }
    /* if we don't have a child, do we have a sibling? */
    else if (curr->next != NULL)
    {
        curr = curr->next;
        for (j = 0; j < (nestLevel - 1); j++)
            printf ("| ");
        if (nestLevel > 0)
            printf ("+ ");
        printAst(curr, nestLevel);
    }
    /* end of the road, go a level*/
    else
    {
        while(curr->parent != NULL && curr->next == NULL)
        {
            curr = curr->parent;
            nestLevel--;
        }
        /* either we're at the root, or we found a new branch */
        if (curr != parseTree)
        {
            for (j = 0; j < (nestLevel - 1); j++)
                printf ("| ");
            if (nestLevel > 0)
                printf ("+ ");
            printAst(curr->next, nestLevel);
        }
        return;
    }

}
