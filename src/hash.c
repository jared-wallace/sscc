/*
 * Hashtable
 */
#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern hash_table_t *global_var_scope;
extern hash_table_t *global_func_scope;

hash_table_t* create_symbol_table(int size)
{
  hash_table_t* new_table;
  if (size < 1) // invalid size
    return NULL;
  if ((new_table = malloc(sizeof(list_t *) * size)) == NULL) // allocate mem for table structure
    return NULL;
  if ((new_table->table = malloc(sizeof(list_t *) * size)) == NULL) // allocate mem for table
    return NULL;
  int i;
  // initialize elements
  for(i = 0; i < size; i++)
    new_table->table[i] = NULL;
  // set size
  new_table->size = size;
  return new_table;
}

int hash(hash_table_t* symbol_table, char* string)
{
  unsigned int hashval = 0;
  for (; *string != '\0'; string++)
    hashval = *string + (hashval << 5) - hashval;
  return hashval % symbol_table->size;
}

/* Hash the string, go to the correct index in the array,
 * and then do a linear search on the linked list that resides there.
 */
list_t *lookup_identifier(hash_table_t *symbol_table, char *string)
{
  list_t *list;
  unsigned int hashval = hash(symbol_table, string);

  for(list = symbol_table->table[hashval]; list != NULL; list = list->next)
  {
    if (strcmp(string, list->string) == 0)
      return list;
  }
  return NULL;
}


/*
 * For looking up new function declarations, we need to compare the entire signature,
 * which for our purposes is the name and the type of the parameters.
 */
list_t *lookup_func_identifier(hash_table_t *symbol_table, char *string, tree *params)
{
  list_t *list;
  unsigned int hashval = hash(symbol_table, string);

  for(list = symbol_table->table[hashval]; list != NULL; list = list->next)
  {
    if (strcmp(string, list->string) == 0)
    {

        if (list->params == NULL && params != NULL)
            return NULL;
        if (list->params != NULL && params == NULL)
            return NULL;
        if (!params)
            return list;
        tree *curr = params->child;
        int i = 0;
        while (curr != NULL)
        {
            if (list->params[i] != curr->type)
                return NULL;
            i++;
            curr = curr->next;
        }
        if (list->num_params != i)
            return NULL;
        return list;
    }
  }
  return NULL;
}


/* Hash the string. Go to the correct place in the array.
 * Insert the new string at the beginning.
 */
int add_identifier(hash_table_t *symbol_table, char *string)
{
  list_t* new_list;
  list_t* current_list;
  unsigned int hashval = hash(symbol_table, string);
  if ((new_list = malloc(sizeof(list_t))) == NULL)
    return 1; // failed allocation
  current_list = lookup_identifier(symbol_table, string);
  if (current_list != NULL)
    return 2; // already present
  new_list->string = strdup(string);
  new_list->next = symbol_table->table[hashval];
  symbol_table->table[hashval] = new_list;
  return 0; // successfully added
}

int add_func_identifier(hash_table_t *symbol_table, tree *type, char *string, tree *params)
{
  list_t* new_list;
  list_t* current_list;
  unsigned int hashval = hash(symbol_table, string);
  if ((new_list = malloc(sizeof(list_t))) == NULL)
    return 1; // failed allocation
  current_list = lookup_func_identifier(symbol_table, string, params);
  if (current_list != NULL)
    return 2; // already present
  /* set id string value */
  new_list->string = strdup(string);
  /* set return type */
  new_list->type = type->val;
  /* set param array */
  if (params != NULL && params->child != NULL)
  {
      /* Start with space for 20 params */
      new_list->params = (int *)calloc(20, sizeof(int));
      if (!new_list->params) return 1;
      tree *curr;
      curr = params->child;
      int i = 0;
      int size = 20;
      while (curr != NULL)
      {
          if (i > size-2)
          {
              int *temp = calloc(size*1.5, sizeof(int));
              if (!temp) return 1;
              memcpy(temp, new_list->params, size);
              free(new_list->params);
              new_list->params = temp;
              size *= 1.5;
          }
          new_list->params[i] = curr->val;
          i++;
          curr = curr->next;
      }
      new_list->num_params = i;
  }
  new_list->next = symbol_table->table[hashval];
  symbol_table->table[hashval] = new_list;
  return 0; // successfully added

}

void dump_hash_table(hash_table_t* hashtable)
{
    int i;
    list_t *list, *temp;

    if (hashtable==NULL)
        return;

    int table;

    if (hashtable == global_func_scope)
        table = 0;
    else if (hashtable == global_var_scope)
        table = 1;
    else
        table = hashtable - global_var_scope;
    switch (table)
    {
        case 0:
            fprintf(stderr, "Function Table\n");
            break;
        case 1:
            fprintf(stderr, "Global Variable Table\n");
            break;
        default:
            fprintf(stderr, "Symbol Table %d\n", table);
    }

    fprintf(stderr, "================================================================================\n");
    for(i = 0; i < hashtable->size; i++)
    {
        list = hashtable->table[i];
        while(list != NULL) {
            switch (list->type)
            {
              case 265:
                  fprintf(stderr, "%s\t", "int");
                  break;
              case 266:
                  fprintf(stderr, "%s\t", "string");
                  break;
              case 267:
                  fprintf(stderr, "%s\t", "char");
                  break;
              case 269:
                  fprintf(stderr, "%s\t", "void");
                  break;
              default:
                  fprintf(stderr, "%s\t", "err");
                  break;
            }
            fprintf(stderr, "%s\t", list->string);
            if (list->arr != 0)
            {
                fprintf(stderr, "Array[");
                if(list->arr == 1)
                    fprintf(stderr, "%d]", list->arr_size);
                else
                    fprintf(stderr, "]");
            }
            if (list->num_params)
            {
                fprintf(stderr, "(");
                int j;
                for (j = 0; j < list->num_params - 1; j++)
                {
                    switch (list->params[j])
                    {
                      case 265:
                          fprintf(stderr, "int, ");
                          break;
                      case 266:
                          fprintf(stderr, "char, ");
                          break;
                      case 267:
                          fprintf(stderr, "void, ");
                          break;
                    }
                }
                switch (list->params[j])
                {
                    case 265:
                      fprintf(stderr, "int");
                      break;
                    case 266:
                      fprintf(stderr, "string");
                      break;
                    case 267:
                      fprintf(stderr, "char");
                      break;
                    case 269:
                      fprintf(stderr, "void");
                      break;
                }
                fprintf(stderr, ")");
            }
            fprintf(stderr, "\n");
            list = list->next;
        }
    }
    fprintf(stderr, "================================================================================\n");
}

void free_table(hash_table_t* hashtable)
{
  int i;
  list_t *list, *temp;

  if (hashtable==NULL)
    return;
  for(i = 0; i < hashtable->size; i++)
  {
    list = hashtable->table[i];
    while(list != NULL) {
      temp = list;
      list = list->next;
      free(temp->string);
      if (temp->params != NULL)
          free(temp->params);
      free(temp);
    }
  }

  free(hashtable->table);
  free(hashtable);
}
