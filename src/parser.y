%{
    #include <stdio.h>
    #include <string.h>
    #include "hash.h"
    #include "tree.h"

    extern int yylineno;                    /* current line number */
    extern int yycol;                       /* current line number */
    extern unsigned int errors;             /* number of errors encountered */
    extern char error_msg[500];             /* error message buffer */
    extern struct treenode *parseTree;      /* root of the AST */
    extern hash_table_t *global_var_scope;  /* global variable table */
    extern hash_table_t *current_scope;     /* current table */
    extern hash_table_t *global_func_scope; /* global function table */
    tree* temp;

    void yyerror(char *);                   /* error handling function */
    int yylex(void);
    void undeclared(char *id);
    void mult_decl(char *id);
%}

%union
{
    int int_val;                        /* integer value */
    char *identifier;                   /* identifier    */
    struct treenode *node;              /* AST node pointer */
};

/* major stuff */
%token <identifier>ID
%token <int_val>INTCONST
%token <int_val>CHARCONST
%token <identifier>STRCONST

/* keywords */
%token KWD_IF
%token KWD_ELSE
%token KWD_WHILE
%token KWD_INT
%token KWD_STRING
%token KWD_CHAR
%token KWD_RETURN
%token KWD_VOID

/* operators */
%token OPER_ADD
%token OPER_SUB
%token OPER_MUL
%token OPER_DIV
%token OPER_LT
%token OPER_GT
%token OPER_GTE
%token OPER_LTE
%token OPER_EQ
%token OPER_NEQ
%token OPER_ASGN

/* brackets & parens */
%token LSQ_BRKT
%token RSQ_BRKT
%token LCRLY_BRKT
%token RCRLY_BRKT
%token LPAREN
%token RPAREN

/* punctuation */
%token COMMA
%token SEMICLN

%token ILLEGAL_TOK
%token ERROR

/* dangling else */
%nonassoc RPAREN
%nonassoc KWD_ELSE

/* declare types */
%type<node> program declList decl varDecl typeSpecifier funDecl
%type<node> formalDeclList formalDecl funBody localDeclList
%type<node> statementList statement compoundStmt assignStmt
%type<node> condStmt loopStmt returnStmt var expression relop
%type<node> addExpr addop term mulop factor funcCallExpr argList

%start program

%%
program         : declList
                {
                    $$ = makeTree(PROGRAM);
                    addChild ($$, $1);
                    parseTree = $$;
                }
                ;
declList        : decl
                {
                    $$ = makeTree(DECLLIST);
                    addChild($$, $1);
                }
                | declList decl
                {
                    addChild ($1, $2);
                    $$ = $1;
                }
                ;
decl            : varDecl
                {
                    $$ = $1;
                    /* global variable */
                    if ( add_identifier(global_var_scope, $1->str_val) == 2)
                        mult_decl($1->str_val);
                    $1->id = lookup_identifier(global_var_scope, $1->str_val);
                    $1->id->scope = global_var_scope;
                    $1->id->type = $1->val;
                    /* set array size */
                    if ($1->arr == 1) /* We have a size value to set */
                        $1->id->arr_size = $1->arr_size;
                    $1->id->arr = $1->arr;


                }
                | funDecl
                { $$ = $1; }
                ;
varDecl         : typeSpecifier ID LSQ_BRKT INTCONST RSQ_BRKT SEMICLN
                {
                    $$ = copyTree(VARDECL, $1);
                    $$->str_val = malloc(strlen($2));
                    strcpy($$->str_val, $2);
                    $$->arr_size = $4;
                    $$->arr = 1;
                }
                | typeSpecifier ID SEMICLN
                {
                    $$ = copyTree(VARDECL, $1);
                    $$->str_val = malloc(strlen($2));
                    strcpy($$->str_val, $2);
                }
                ;
typeSpecifier   : KWD_INT
                {
                    $$ = makeTreeWithIntVal (TYPESPECIFIER, KWD_INT);
                }
                | KWD_CHAR
                {
                    $$ = makeTreeWithIntVal (TYPESPECIFIER, KWD_CHAR);
                }
                | KWD_VOID
                {
                    $$ = makeTreeWithIntVal (TYPESPECIFIER, KWD_VOID);
                }
                ;
funDecl         : typeSpecifier ID LPAREN
                {
                    /* set "current scope" to be the new table */
                    current_scope = create_symbol_table(128);
                    /* add new scope table to the scope tree */
                    if (global_var_scope->child == NULL)
                        global_var_scope->child = current_scope;
                    else
                    {
                        hash_table_t *curr;
                        curr = global_var_scope->child;
                        while (curr->next != NULL)
                            curr = curr->next;
                        curr->next = current_scope;
                    }
                }
                  formalDeclList RPAREN
                  {
                    temp = copyTree(FUNDECL, $1);
                    temp->scope_table = current_scope;
                    /* Hash the function identifier */
                    if (add_func_identifier(global_func_scope, $1, $2, $5) == 2)
                        mult_decl($2);
                    /* and set our "id" field appropriately */
                    temp->id = lookup_func_identifier(global_func_scope, $2, $5);
                    /* go ahead and set return type in the node (hash table add function
                    takes care of adding it to the symbol table */
                    temp->type = $1->val;
                    /* Add the formaldecllist node as a child */
                    addChild(temp, $5);
                  }
                  funBody
                {
                    $$ = temp;
                    /* Add our function body as a child */
                    addChild($$, $8);

                }
                | typeSpecifier ID LPAREN
                {
                    /* set "current scope" to be the new table */
                    current_scope = create_symbol_table(128);
                    /* add new scope table to the scope tree */
                    if (global_var_scope->child == NULL)
                        global_var_scope->child = current_scope;
                    else
                    {
                        hash_table_t *curr;
                        curr = global_var_scope->child;
                        while (curr->next != NULL)
                            curr = curr->next;
                        curr->next = current_scope;
                    }
                }
                RPAREN
                {
                    temp = copyTree(FUNDECL, $1);
                    temp->scope_table = current_scope;
                    /* Hash the function identifier */
                    if (add_func_identifier(global_func_scope, $1, $2, NULL) == 2)
                        mult_decl($2);
                    /* and set our "id" field appropriately */
                    temp->id = lookup_func_identifier(global_func_scope, $2, NULL);
                    /* go ahead and set return type in the node (hash table add function
                    takes care of adding it to the symbol table */
                    temp->type = $1->val;
                }
                funBody
                {
                    $$ = temp;
                    addChild($$, $7);
                }
                ;
formalDeclList  : formalDecl
                {
                    $$ = makeTree(FORMALDECLLIST);
                    addChild($$, $1);
                }
                | formalDeclList COMMA formalDecl
                {
                    addChild ($1, $3);
                    $$ = $1;
                }
                ;
formalDecl      : typeSpecifier ID
                {
                    $$ = copyTree(FORMALDECL, $1);
                    $$->str_val = malloc(strlen($2));
                    strcpy($$->str_val, $2);
                    /* hash */
                    if (add_identifier(current_scope, $$->str_val) == 2)
                        mult_decl($$->str_val);
                    $$->id = lookup_identifier(current_scope, $$->str_val);
                    $$->id->scope = current_scope;
                    $$->type = $1->val;
                    $$->id->type = $1->val;
                }
                | typeSpecifier ID LSQ_BRKT RSQ_BRKT
                {
                    $$ = copyTree(FORMALDECL, $1);
                    $$->str_val = malloc(strlen($2));
                    strcpy($$->str_val, $2);
                    $$->arr = 2;
                    /* hash */
                    if (add_identifier(current_scope, $$->str_val) == 2)
                        mult_decl($$->str_val);
                    $$->id = lookup_identifier(current_scope, $$->str_val);
                    $$->id->scope = current_scope;
                    $$->id->arr = 2;
                    $$->type = $1->val;
                    $$->id->type = $1->val;

                }
                ;
funBody         : LCRLY_BRKT localDeclList statementList RCRLY_BRKT
                {
                    $$ = makeTree(FUNBODY);
                    addChild($$, $2);
                    addChild($$, $3);
                }
                ;
localDeclList   : { $$ = makeTree(LOCALDECLLIST); }
                | localDeclList varDecl
                {
                    addChild($1, $2);
                    /* hash */
                    if (add_identifier(current_scope, $2->str_val) == 2) /* We only check locally, since shadowing globals is acceptable */
                        mult_decl($2->str_val);
                    $2->id = lookup_identifier(current_scope, $2->str_val);
                    /* set scope */
                    $2->id->scope = current_scope;
                    /* set type value */
                    switch ($2->val)
                    {
                        case KWD_INT:
                        case KWD_CHAR:
                        case KWD_STRING:
                            $2->id->type = $2->val;
                            $2->type = $2->val;
                            break;
                    }
                    /* set array size */
                    if ($2->arr == 1) /* We have a size value to set */
                        $2->id->arr_size = $2->arr_size;
                    $2->id->arr = $2->arr;
                    $$ = $1;
                }
                ;
statementList   : { $$ = makeTree(STATEMENTLIST); }
                | statementList statement
                {
                    addChild($1, $2);
                    $$ = $1;
                }
                ;
statement       : compoundStmt
                { $$ = $1; }
                | assignStmt
                { $$ = $1; }
                | condStmt
                { $$ = $1; }
                | loopStmt
                { $$ = $1; }
                | returnStmt
                { $$ = $1; }
                ;
compoundStmt    : LCRLY_BRKT statementList RCRLY_BRKT
                { $$ = $2; }
                ;
assignStmt      : var OPER_ASGN expression SEMICLN
                {
                    $$ = makeTree(ASSIGNSTMT);
                    addChild($$, $1);
                    addChild($$, $3);
                    /* check type */
                    if ($1->type != $3->type)
                    {
                        fprintf(stderr, "\nType1: %d\n", $1->type);
                        fprintf(stderr, "\nType2: %d\n", $3->type);
                        sprintf(error_msg, "Illegal typing\n");
                        yyerror(error_msg);
                    }
                }
                | expression SEMICLN
                { $$ = $1; }
                ;
condStmt        : KWD_IF LPAREN expression RPAREN statement
                {
                    $$ = makeTree(CONDSTMT);
                    addChild($$, $3);
                    addChild($$, $5);
                }
                | KWD_IF LPAREN expression RPAREN statement KWD_ELSE statement
                {
                    $$ = makeTree(CONDSTMT);
                    addChild($$, $3);
                    addChild($$, $5);
                    addChild($$, $7);
                }
                ;
loopStmt        : KWD_WHILE LPAREN expression RPAREN statement
                {
                    $$ = makeTree(LOOPSTMT);
                    addChild($$, $3);
                    addChild($$, $5);
                }
                ;
returnStmt      : KWD_RETURN SEMICLN
                { $$ = makeTree(RETURNSTMT); }
                | KWD_RETURN expression SEMICLN
                {
                    $$ = makeTree(RETURNSTMT);
                    /* try trimming */
                    /*if ($2->type == KWD_INT)*/
                    /*{*/
                        /*$$->type = KWD_INT;*/
                        /*$$->val = $2->val;*/
                    /*}*/
                    /*else*/
                        addChild($$, $2);
                }
                ;
var             : ID
                {
                    $$ = makeTreeWithIDVal(VAR, $1);
                    /* try looking up in local scope */
                    $$->id = lookup_identifier(current_scope, $1);
                    if ($$->id == NULL)
                    {
                        /* since it was not declared locally, try global lookup */
                        $$->id = lookup_identifier(global_var_scope, $1);
                        if ($$->id == NULL)
                            undeclared($1);
                    }
                    $$->type = $$->id->type;
                }
                | ID LSQ_BRKT addExpr RSQ_BRKT
                {
                    $$ = makeTreeWithIDVal(VAR, $1);
                    $$->arr = 1;
                    /* check for non-int */
                    if ($3->type != KWD_INT || $3->nodeKind != FACTOR)
                    {
                        sprintf(error_msg, "Invalid array index");
                        yyerror(error_msg);
                    }
                    addChild($$, $3);
                    /* try looking up in local scope */
                    $$->id = lookup_identifier(current_scope, $1);
                    if ($$->id == NULL)
                    {
                        /* since it was not declared locally, try global lookup */
                        $$->id = lookup_identifier(global_var_scope, $1);
                        if ($$->id == NULL)
                            undeclared($1);
                    }
                    /* For arrays, check bounds */
                    if ($$->id->arr == 1)
                    {
                        if ($$->child->val > $$->id->arr_size)
                        {
                            sprintf(error_msg, "Array index out of bounds");
                            yyerror(error_msg);
                        }
                    }
                    $$->type = $$->id->type;
                }
                ;
expression      : addExpr
                { $$ = $1; }
                | expression relop addExpr
                {
                    $$ = makeTree(EXPRESSION);
                    addChild($$, $1);
                    addChild($$, $2);
                    addChild($$, $3);
                }
                ;
relop           : OPER_LTE
                {
                    $$ = makeTreeWithIntVal(RELOP, OPER_LTE);
                }
                | OPER_LT
                {
                    $$ = makeTreeWithIntVal(RELOP, OPER_LT);
                }
                | OPER_GT
                {
                    $$ = makeTreeWithIntVal(RELOP, OPER_GT);
                }
                | OPER_GTE
                {
                    $$ = makeTreeWithIntVal(RELOP, OPER_GTE);
                }
                | OPER_EQ
                {
                    $$ = makeTreeWithIntVal(RELOP, OPER_EQ);
                }
                | OPER_NEQ
                {
                    $$ = makeTreeWithIntVal(RELOP, OPER_NEQ);
                }
                ;
addExpr         : term
                { $$ = $1; }
                | addExpr addop term
                {
                    $$ = makeTree(ADDEXPR);
                    /* try to fold if we have two ints */
                    if ($1->nodeKind == FACTOR 
                        && $3->nodeKind == FACTOR
                        && $1->type == KWD_INT
                        && $3->type == KWD_INT)
                    {
                        $$ = $1;
                        if ($2->val == OPER_ADD)
                            $$->val = $1->val + $3->val;
                        else
                            $$->val = $1->val - $3->val;
                    }
                    else
                    {
                        if ($1->type == $3->type)
                            $$->type = $1->type;
                        addChild($$, $1);
                        addChild($$, $2);
                        addChild($$, $3);
                    }
                }
                ;
addop           : OPER_ADD
                { $$ = makeTreeWithIntVal(ADDOP, OPER_ADD); }
                | OPER_SUB
                { $$ = makeTreeWithIntVal(ADDOP, OPER_SUB); }
                ;
term            : factor
                { $$ = $1; }
                | term mulop factor
                {
                    $$ = makeTree(TERM);
                    /* try to fold if we have two ints */
                    if ($1->type == KWD_INT 
                        && $3->type == KWD_INT
                        && $1->nodeKind == FACTOR
                        && $3->nodeKind == FACTOR)
                    {
                        $$ = $1;
                        if ($2->val == OPER_MUL)
                            $$->val = $1->val * $3->val;
                        else
                            $$->val = $1->val / $3->val;
                    }
                    else
                    {
                        if ($1->type == $3->type)
                            $$->type = $1->type;
                        addChild($$, $1);
                        addChild($$, $2);
                        addChild($$, $3);
                    }
                }
                ;
mulop           : OPER_MUL
                { $$ = makeTreeWithIntVal(MULOP, OPER_MUL); }
                | OPER_DIV
                { $$ = makeTreeWithIntVal(MULOP, OPER_DIV); }
                ;
factor          : LPAREN expression RPAREN
                { $$ = $2; }
                | var
                { $$ = $1; }
                | funcCallExpr
                { $$ = $1; }
                | INTCONST
                {
                    $$ = makeTreeWithIntVal(FACTOR, $1);
                    $$->type = KWD_INT;
                }
                | CHARCONST
                {
                    $$ = makeTreeWithIntVal(FACTOR, $1);
                    $$->type = KWD_CHAR;
                }
                | STRCONST
                {
                    $$ = makeTree(FACTOR);
                    $$->str_val = $1;
                    $$->type = KWD_STRING;
                }
                ;
funcCallExpr    : ID LPAREN argList RPAREN
                {
                    // hack for "output" function
                    if (!strcmp($1, "output"))
                    {
                        // make new "output" node
                        $$ = makeTree(OUTPUT);
                        addChild($$, $3);
                    }
                    else
                    {
                        $$ = makeTree(FUNCCALLEXPR);
                        addChild($$, $3);
                        $$->str_val = malloc(strlen($1));
                        strcpy($$->str_val, $1);
                        $$->id = lookup_func_identifier(global_func_scope, $$->str_val, $$->child);
                        if (!$$->id)
                            undeclared($$->str_val);
                        $$->type = $$->id->type;
                    }
                }
                | ID LPAREN RPAREN
                {
                    $$ = makeTree(FUNCCALLEXPR);
                    $$->str_val = malloc(strlen($1));
                    strcpy($$->str_val, $1);
                    $$->id = lookup_func_identifier(global_func_scope, $$->str_val, NULL);
                    if (!$$->id)
                        undeclared($$->str_val);
                    $$->type = $$->id->type;
                }
                ;
argList         : expression
                {
                    $$ = makeTree(ARGLIST);
                    addChild($$, $1);
                }
                | argList COMMA expression
                {
                    addChild($1, $3);
                    $$ = $1;
                }
                ;
%%

void yyerror(char *msg)
{
    fprintf(stderr, "line %d: %s\n", yylineno, msg);
    errors++;
    exit(1);
}

void mult_decl(char *id)
{
    sprintf(error_msg, "Multiple declaration: <%s>", id);
    yyerror(error_msg);
}

void undeclared(char *id)
{
    sprintf(error_msg, "<%s> is undeclared", id);
    yyerror(error_msg);
}

