#ifndef CODEGEN_H
#define CODEGEN_H

#include <stdio.h>
#include <stdlib.h>
#include "hash.h"
#include "tree.h"

#define v0 2        /* return val */
#define v1 3        /* return val */
#define a0 4        /* func call param */
#define a1 5        /* func call param */
#define a2 6        /* func call param */
#define a3 7        /* func call param */
#define t0 8        /* temp, don't preserve */
#define t1 9        /* temp, don't preserve */
#define t2 10       /* temp, don't preserve */
#define t3 11       /* temp, don't preserve */
#define t4 12       /* temp, don't preserve */
#define t5 13       /* temp, don't preserve */
#define t6 14       /* temp, don't preserve */
#define t7 15       /* temp, don't preserve */
#define s0 16       /* func variable, must preserve */
#define s1 17       /* func variable, must preserve */
#define s2 18       /* func variable, must preserve */
#define s3 19       /* func variable, must preserve */
#define s4 20       /* func variable, must preserve */
#define s5 21       /* func variable, must preserve */
#define s6 22       /* func variable, must preserve */
#define s7 23       /* func variable, must preserve */
#define t8 24       /* temp, don't preserve */
#define t9 25       /* temp, don't preserve */
#define gp 28       /* global pointer */
#define sp 29       /* stack pointer */
#define fp 30       /* frame pointer */
#define ra 31       /* return address */

#define OPER_ADD 270
#define OPER_SUB 271
#define OPER_MUL 272
#define OPER_DIV 273
#define OPER_EQ 278
#define OPER_LT 274
#define OPER_GT 275

// assembly printing format options
#define regular 0       /* inst reg1, reg2, reg3 */
#define immediate 1     /* inst reg1, reg2, immediate */
#define load 2          /* inst reg1, immediate(reg3) */
#define label 4         /* inst: */
#define divide 5        /* div reg1, reg2 */
#define mflo 6          /* mflo reg1 */
#define globalLoad 7    /* lw reg1, inst */
#define move 8          /* mv reg2, reg3 */
#define branch 9        /* inst reg1, reg2, offset */
#define loadAddress 10  /* inst reg1, offset */
#define jumpLink 11     /* inst offset */
#define jump 12         /* inst reg1 */
#define syscall 13      /* inst */

/* assembly node structure */
typedef struct assemblynode asm_node;
struct assemblynode
{
    char *inst;     /* instruction */
    int reg1;       /* first register */
    int reg2;       /* second register */
    int reg3;       /* third register */
    int immed;      /* immediate value or offset */
    char *offset;   /* text offset for global variables */
    int type;       /* inst format */
    asm_node *next; /* next instruction */
};

/* Register structure */
typedef struct registernode reg_node;
struct registernode
{
    int occupied;       /* Whether the register is live */
    int type;           /* One for variable, two for literal */
    list_t *contents;   /* The pointer to the variable struct */
};

int is_global(tree *node);
int count_locals(tree *node);
int codeGen(tree *root);
int expr(tree *node);
int loop(tree *node, FILE *filep);
int assignment(tree *node);
int funcCall(tree *node);
int registerAllocate(asm_node *start);
int print(FILE *filep);
tree* left_child(tree *node);
tree* right_child(tree *node);
tree* middle_child(tree *node);
tree *far_right_child(tree *node);
int next_reg();
int base(tree *node);
int offset(tree *node);
int get_val(tree *node);
int not_allocated(tree *node);
void add_asm_node(asm_node *node);
char* get_op(tree* node);
int get_reg(tree *node);
void print_asm();
void func_decl(tree *node, FILE *filep);
void fun_body(tree *node, FILE *filep);
void setup(tree *node, FILE *filep);
int generate_asm_nodes(tree *node, FILE *filep);
tree *get_body(tree *node);
void assign(tree *node, FILE *filep);
void conditional(tree *node, FILE *filep);
void handle_globals(tree *node, FILE *filep);
void handle_locals(tree *node, FILE *filep);
void free_reg(int reg);
char *new_label();
int func_call(tree *node);
void output(tree *node, FILE *filep);
void retrn(tree *node);
void generate_asm_node(char *inst, int reg1, int reg2, int reg3, int immed, char *offset, int type);
#endif
