#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"
#include "tree.h"
#include "codegen.h"

extern tree *parseTree;
extern int debug;
extern int print_std;
extern hash_table_t *global_var_scope;
asm_node *asm_start;        /* The start of the assembly instruction linked list */
asm_node *asm_curr;         /* Current end of the assembly instruction linked list */
reg_node registers[32];     /* Our register data structure (occupied, type, contents) */
int label_num = 0;

/*
 * Entrance point for code generation
 * Accepts: node representing the root of the tree
 * Returns: 0 for an invalid node, 1 for successful codegen
 */
int codeGen(tree *node)
{
    FILE *filep = fopen("out.s", "wb");
    if (!filep)
    {
        return 0;
    }
    setup(node, filep);
    generate_asm_nodes(node, filep);
    print(filep);
    return 1;
}

/*
 * Generates a linked list of assembly node structures.
 * Recurses through the tree until it finds a matching node.
 * Can also be called by other functions for sub-processing.
 *
 * Accepts: node representing the root of some tree, and a
 *  FILE pointer to the resultant file.
 *
 */
int generate_asm_nodes(tree *node, FILE *filep)
{
    tree *curr;
    switch (node->nodeKind)
    {
        case OUTPUT:
            output(node, filep);
            return 0;
        case FUNDECL:
            func_decl(node, filep);
            return 0;
        case FUNCCALLEXPR:
            return func_call(node);
        case ADDEXPR:
        case TERM:
            return expr(node);
        case ASSIGNSTMT:
            assign(node, filep);
            return 0;
        case CONDSTMT:
            conditional(node, filep);
            return 0;
        case LOOPSTMT:
            loop(node, filep);
            return 0;
        case STATEMENTLIST:
            curr = node->child;
            while (curr)
            {
                generate_asm_nodes(curr, filep);
                curr = curr->next;
            }
            return 0;
        case RETURNSTMT:
            retrn(node);
            return 0;
    }
    curr = node;

    /* if we have a child */
    if (curr->child != NULL)
    {
        curr = curr->child;
        generate_asm_nodes(curr, filep);
    }
    /* if we don't have a child, do we have a sibling? */
    else if (curr->next != NULL)
    {
        curr = curr->next;
        generate_asm_nodes(curr, filep);
    }
    /* end of the road, go a level*/
    else
    {
        while(curr->parent != NULL && curr->next == NULL)
        {
            curr = curr->parent;
        }
        /* either we're at the root, or we found a new branch */
        if (curr != parseTree)
        {
            generate_asm_nodes(curr->next, filep);
        }
        return 0;
    }

}

/*
 * Processes expressions.
 *
 * Accepts: A node representing the root of some tree.
 *
 * Returns: An integer representing the register where the result
 *  is stored.
 */
int expr(tree *node)
{
    if (debug)
        fprintf(stdout, "Inside expr\n");
    int result;
    int tmp1, tmp2, tmp3;
    switch (node->nodeKind)
    {
        case FUNCCALLEXPR:
            result = func_call(node);
            break;
        case EXPRESSION:
            tmp1 = expr( left_child(node) );
            tmp2 = expr( far_right_child(node) );
            switch (middle_child(node)->val)
            {
                case OPER_LT:
                    result = next_reg();
                    generate_asm_node("slt", result, tmp1, tmp2, 0, NULL, regular);
                    free_reg(tmp1);
                    free_reg(tmp2);
                    break;
                case OPER_GT:
                    result = next_reg();
                    generate_asm_node("slt", result, tmp2, tmp1, 0, NULL, regular);
                    free_reg(tmp1);
                    free_reg(tmp2);
                    break;
            }
            break;
        case ADDEXPR:
        case TERM:
            tmp2 = 0; /* register2 */
            tmp3 = 0; /* register3 */
            if (debug)
                fprintf(stdout, "Inside TERM\n");
            result = next_reg();                        /* destination */
            registers[ result ].type = 2;
            tmp2 = expr( left_child(node) );
            tmp3 = expr( far_right_child(node) );
            // Division is handled differently, we need an mflo
            if (middle_child(node)->val == OPER_DIV)
            {
                if (debug)
                    fprintf(stdout, "Inside OPER_DIV\n");
                generate_asm_node(get_op(middle_child(node)), 0, tmp2, tmp3, 0, NULL, divide);
                generate_asm_node("mflo", result, 0, 0, 0, NULL, mflo);
            }
            else
            {
                generate_asm_node(get_op(middle_child(node)), result, tmp2, tmp3, 0, NULL, regular);
            }
            free_reg(tmp2);
            free_reg(tmp3);
            break;
        case VAR:
            if ( not_allocated(node) )
            {
                if (debug)
                {
                    fprintf(stdout, "Inside VAR not allocated\n");
                    fprintf(stdout, "with node allocation = %d\n", node->id->allocated);
                }
                tmp1 = 0; /* inst type */
                tmp2 = 0; /* register2 */
                tmp3 = 0; /* immediate value */
                if ( is_global(node) )
                {
                    tmp1 = globalLoad;
                }
                else
                {
                    tmp2 = base(node);    /* the register with the mem address */
                    tmp3 = offset(node); /* the offset from that address */
                    tmp1 = load;
                }
                result = next_reg();
                generate_asm_node("lw", result, tmp2, 0, tmp3, node->id->string, tmp1);
                node->id->allocated = 1;
                node->id->reg = result;
            }
            else
            {
                result = get_reg(node);
                if (debug)
                    fprintf(stdout, "Inside VAR allocated, reg = %d\n", result);
            }
            break;
        case FACTOR:
            result = next_reg();
            registers[ result ].type = 2;
            generate_asm_node("addi", result, 0, 0, get_val(node), NULL, immediate);
            break;
    }
    return result;
}

/*
 * The function that prints the completed assembly node linked list.
 * It currently prints to stdout as well as the file.
 */
int print(FILE *filep)
{
    if (debug)
    {
        fprintf(stdout, "Inside print\n");
        fprintf(stdout, "Start: %x\n", asm_start);
        fprintf(stdout, "Next: %x\n", asm_start->next);
    }
    asm_curr = asm_start;
    /*
     * There are a deplorable variety of instruction formats :(
     */
    for (; asm_curr; asm_curr = asm_curr->next)
    {
        switch (asm_curr->type)
        {
            case regular:
                fprintf(filep, "\t\t%s $%d, $%d, $%d\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->reg2, asm_curr->reg3);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, $%d, $%d\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->reg2, asm_curr->reg3);
                break;
            case immediate:
                fprintf(filep, "\t\t%s $%d, $%d, %d\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->reg2, asm_curr->immed);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, $%d, %d\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->reg2, asm_curr->immed);
                break;
            case load:
                fprintf(filep, "\t\t%s $%d, %d($%d)\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->immed, asm_curr->reg2);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, %d($%d)\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->immed, asm_curr->reg2);
                break;
            case loadAddress:
                fprintf(filep, "\t\t%s $%d, %s\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->offset);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, %s\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->offset);
                break;
            case globalLoad:
                fprintf(filep, "\t\t%s $%d, %s\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->offset);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, %s\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->offset);
                break;
            case mflo:
                fprintf(filep, "\t\t%s $%d\n", asm_curr->inst,
                        asm_curr->reg1);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d\n", asm_curr->inst,
                        asm_curr->reg1);
                break;
            case divide:
                fprintf(filep, "\t\t%s $%d, $%d\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->reg2);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, $%d\n", asm_curr->inst,
                        asm_curr->reg1, asm_curr->reg2);
                break;
            case label:
                fprintf(filep, "%s:\n", asm_curr->offset);
                if (print_std)
                    fprintf(stdout, "%s:\n", asm_curr->offset);
                break;
            case move:
                fprintf(filep, "\t\t%s $%d, $%d\n", asm_curr->inst,
                        asm_curr->reg2, asm_curr->reg3);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, $%d\n", asm_curr->inst,
                        asm_curr->reg2, asm_curr->reg3);
                break;
            case branch:
                fprintf(filep, "\t\t%s $%d, $%d, %s\n", asm_curr->inst,
                        asm_curr->reg2, asm_curr->reg3, asm_curr->offset);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d, $%d, %s\n", asm_curr->inst,
                        asm_curr->reg2, asm_curr->reg3, asm_curr->offset);
                break;
            case jumpLink:
                fprintf(filep, "\t\t%s %s\n", asm_curr->inst, asm_curr->offset);
                if (print_std)
                    fprintf(stdout, "\t\t%s %s\n", asm_curr->inst, asm_curr->offset);
                break;
            case jump:
                fprintf(filep, "\t\t%s $%d\n", asm_curr->inst, asm_curr->reg1);
                if (print_std)
                    fprintf(stdout, "\t\t%s $%d\n", asm_curr->inst, asm_curr->reg1);
                break;
            case syscall:
                fprintf(filep, "\t\t%s\n", asm_curr->inst);
                if (print_std)
                    fprintf(stdout, "\t\t%s\n", asm_curr->inst);
                break;
        }
    }

}

/* Return left-most child */
tree* left_child(tree *node)
{
    return node->child;
}

/* return right-hand child (only for binary trees) */
tree* right_child(tree *node)
{
    return node->child->next;
}

/* Return middle child of ternary trees */
tree *middle_child(tree *node)
{
    return left_child(node)->next;
}

/* Return right-most child (only for ternary trees) */
tree *far_right_child(tree *node)
{
    return middle_child(node)->next;
}

/*
 * Returns an available register for use. This compiler only
 * uses three different registers for temporary use, at the cost
 * of a great deal of efficiency.
 *
 * Note: next_reg() only marks the register as occupied, the caller must
 * handle type assignment.
 */
int next_reg()
{
    int avail_regs[3] = {t7, t8, t9};
    int i;
    for (i = 0; i < 3; i++)
    {
        if (registers[ avail_regs[i] ].occupied == 0)
        {
            registers[ avail_regs[i] ].occupied = 1;
            return avail_regs[i];
        }
    }
    // if we're here, we have a memory leak
    fprintf(stdout, "Register overflow\n");
    return 0;
}

/* Free the provided register up for future use */
void free_reg(int reg)
{
    // are we trying to free a local variable?
    if (reg >= 8 && reg <= 14)
        return;
    // are we freeing an identifer that needs to be written back?
    if (registers[reg].type == 1)
    {
        int offset = registers[reg].contents->offset;
        asm_node *line = malloc(sizeof(asm_node));
        line->inst = "sw";
        line->reg1 = reg;
        line->immed = offset;
        line->reg2 = sp;
        line->type = load;
        add_asm_node(line);
    }
    registers[reg].type = 0;
    registers[reg].occupied = 0;
    registers[reg].contents = NULL;
}

/* I can't think when this would ever NOT be the stack pointer */
int base(tree *node)
{
    return sp;
}

/* returns the offset from the SP */
int offset(tree *node)
{
    return node->id->offset;
}

/* returns the integer value of a node */
int get_val(tree *node)
{
    return node->val;
}

/* returns 1 if the variable in question has not yet been allocated,
 * 0 otherwise.
 */
int not_allocated(tree *node)
{
    if (node->id->allocated)
        return 0;
    return 1;
}

/* Adds a new assembly node to the linked list */
void add_asm_node(asm_node *node)
{
    if (!asm_start)
    {
        if (debug)
            fprintf(stdout, "add_asm_node first, inst: %s\n", node->inst);
        asm_start = node;
        asm_curr = asm_start;
    }
    else
    {
        if (debug)
        {
            fprintf(stdout, "add_asm_node, inst: %s\n", node->inst);
            print_asm();
        }
        asm_curr->next = node;
        asm_curr = node;
    }
}

/* Determines the operator */
char* get_op(tree *node)
{
    char *result = malloc(4);
    switch (node->val)
    {
        case OPER_ADD:
            result = "add";
            break;
        case OPER_MUL:
            result = "mul";
            break;
        case OPER_SUB:
            result = "sub";
            break;
        case OPER_DIV:
            result = "div";
    }
    return result;
}

/* Returns the register a variable is stored in */
int get_reg(tree *node)
{
    return node->id->reg;
}

/* Used along with gdb for debugging */
void print_asm()
{
    fprintf(stdout, "====================================\n");
    fprintf(stdout, "============start list==============\n");
    fprintf(stdout, "====================================\n");
    asm_node *curr = asm_start;
    while (curr)
    {
        fprintf(stdout, "self: %x\n", curr);
        fprintf(stdout, "inst: %s\n", curr->inst);
        fprintf(stdout, "reg1: %d\n", curr->reg1);
        fprintf(stdout, "reg2: %d\n", curr->reg2);
        fprintf(stdout, "reg3: %d\n", curr->reg3);
        fprintf(stdout, "immed: %d\n", curr->immed);
        fprintf(stdout, "offset: %s\n", curr->offset);
        fprintf(stdout, "type: %d\n", curr->type);
        fprintf(stdout, "next: %x\n", curr->next);
        fprintf(stdout, "====================================\n");
        curr = curr->next;
    }
}

/*
 * Sets up stack frame, and handles parameters.
 * Also handles stack frame teardown.
 * */
void func_decl(tree *node, FILE *filep)
{
    if (debug)
        fprintf(stdout, "Inside func_decl, function: %s\n", node->id->string);
    fflush(stdout);
    // label
    generate_asm_node(NULL, 0, 0, 0, 0, node->id->string, label);
    // See how many locals we need to make room for
    int extra;
    extra = count_locals(node->child->child);
    if (debug)
        fprintf(stdout, "After count_locals call \n");
    fflush(stdout);
    extra = extra * 4;
    // check whether we need padding
    if ((extra) % 8)
        extra = extra + (extra % 8);
    if (debug)
        fprintf(stdout, "Extra equals %d \n", extra);
    // Create and push stack frame with a size of 48 + extra, (4 args, ra, 7 saved registers)
    generate_asm_node("addi", sp, sp, 0, -48 - extra, NULL, immediate);
    // store ra
    generate_asm_node("sw", ra, sp, 0, 44, NULL, load);
    // parameters
    tree *grandchild = node->child->child;
    if (grandchild->nodeKind == FORMALDECL)
    {
        grandchild->id->allocated = 1;
        grandchild->id->reg = a0;
    }
    // body
    fun_body(get_body(node), filep);
    // restore ra
    generate_asm_node("lw", ra, sp, 0, 44, NULL, load);
    // restore stack
    generate_asm_node("addi", sp, sp, 0, 48 + extra, NULL, immediate);
    // jump ra (if we're not main)
    if (strcmp(node->id->string, "main"))
    {
        generate_asm_node("jr", ra, 0, 0, 0, NULL, jump);
    }
    else
    {
        generate_asm_node("addi", v0, 0, 0, 10, NULL, immediate);
        generate_asm_node("syscall", 0, 0, 0, 0, NULL, syscall);
    }
    // If we have more functions in our program, handle them
    if (node->next != NULL)
        generate_asm_nodes(node->next, filep);
}

/* Handle local variables and function body */
void fun_body(tree *node, FILE *filep)
{
    if (node->child->nodeKind == LOCALDECLLIST)
        handle_locals(node->child, filep);
    generate_asm_nodes(node, filep);
}

/* program setup */
void setup(tree *node, FILE *filep)
{
    int i;
    /* Zero out registers */
    for (i = 0; i < 32; i++)
    {
        registers[i].type = 0;
        registers[i].occupied = 0;
        registers[i].contents = NULL;
    }
    fprintf(filep, "##########################################\n");
    if (print_std)
        fprintf(stdout, "##########################################\n");
    // Global variable section
    fprintf(filep, "\t.data\n");
    if (print_std)
        fprintf(stdout, "\t.data\n");
    // Handle globals, if we have any
    handle_globals(node, filep);
    // Setup main as entrance
    fprintf(filep, "\t.globl main\n");
    if (print_std)
        fprintf(stdout, "\t.globl main\n");
    // Instruction section
    fprintf(filep, "\t.text\n");
    if (print_std)
        fprintf(stdout, "\t.text\n");
}

/* skip parameters and just return the function body */
tree *get_body(tree *node)
{
    if (debug)
        fprintf(stdout, "Inside get_body, type: %d\n", node->child->nodeKind);
    fflush(stdout);
    if (node->child->nodeKind != FUNBODY)
        return node->child->next;
    return node->child;
}

/* Handle assignment statements */
void assign(tree *node, FILE *filep)
{
    asm_node *line;
    line = malloc(sizeof(asm_node));
    // evaluate right hand side
    if (right_child(node)->nodeKind == FUNCCALLEXPR)
        line->reg1 = func_call(right_child(node));
    else
        line->reg1 = expr(right_child(node));
    // if the left hand var is not in a register, we store
    if ( not_allocated(left_child(node)) )
    {
        line->inst = "sw";
        if (is_global(left_child(node)))
        {
            if (debug)
                fprintf(stdout, "assigning global\n");
            line->reg2 = gp;
            line->offset = left_child(node)->id->string;
            line->type = globalLoad;
        }
        else
        {
            line->reg2 = base(left_child(node));
            line->immed = offset(left_child(node));
            line->type = load;
        }
        free_reg(line->reg1);
    }
    // otherwise, we just move
    else
    {
        line->inst = "move";
        line->reg2 = get_reg(left_child(node));
        line->reg3 = line->reg1;
        line->type = move;
    }
    free_reg(line->reg3);
    add_asm_node(line);
}

/* Handle iterative code */
int loop(tree *node, FILE *filep)
{
    char *label1, *label2, *label3;
    // get label, print uncond. branch
    label1 = new_label();
    generate_asm_node("beq", 0, 0, 0, 0, label1, branch);
    // get new label, print label
    label2 = new_label();
    generate_asm_node(NULL, 0, 0, 0, 0, label2, label);
    // print loop body
    generate_asm_nodes(right_child(node), filep);
    // print label for condition
    generate_asm_node(NULL, 0, 0, 0, 0, label1, label);
    // print condition
    int result;
    int temp;
    if ( middle_child( left_child(node) )->val != OPER_EQ)
    {
        // less than or greater than
        temp = expr(left_child(node));
        result = next_reg();
        registers[ result ].type = 2;
        // load immediate for our comparison value
        generate_asm_node("addi", result, 0, 0, 1, NULL, immediate);
        free_reg(result);
    }
    else
    {
        result = expr(left_child( left_child(node) ));
        temp = expr(far_right_child( left_child(node) ));
    }
    generate_asm_node("beq", 0, result, temp, 0, label2, branch);
    free_reg(result);
    free_reg(temp);
}

/* Handle conditional code */
void conditional(tree *node, FILE *filep)
{
    char *label1, *label2, *label3;
    int result;
    int temp;
    asm_node *line = malloc(sizeof(asm_node));

    if ( right_child( left_child(node) )->val != OPER_EQ)
    {
        // less than or greater than
        temp = expr(left_child(node));
        result = next_reg();
        registers[ result ].type = 2;
        // load immediate for our comparison value
        generate_asm_node("addi", result, 0, 0, 1, NULL, immediate);
    }
    else
    {
        result = expr(left_child( left_child(node) ));
        temp = expr(far_right_child( left_child(node) ));
    }
    label1 = new_label(); // label1 is how we jump past the "if" code
    // bne on comparison
    generate_asm_node("bne", 0, result, temp, 0, label1, branch);
    // We're done with the comparison, so dump registers
    free_reg(result);
    free_reg(temp);
    // print our "if" code
    generate_asm_nodes(middle_child(node), filep);
    // handle the else code, but only if it actually exists
    label2 = new_label();
    if ( far_right_child(node) )
    {
        // jump past the else code
        generate_asm_node("beq", 0, 0, 0, 0, label2, branch);
    }
    /* print label1, which is either the start of the else code or the rest
     * of the program
     */
    generate_asm_node(NULL, 0, 0, 0, 0, label1, label);
    if ( far_right_child(node) )
    {
        // actually generate else code
        generate_asm_nodes(far_right_child(node), filep);
    }
    // print label2, which is where we jump to after executing the "if" code
    generate_asm_node(NULL, 0, 0, 0, 0, label2, label);
}

/* Generate a new label, which is simply Ln, where n is some unique number.
 * I suppose we should note here that an excessive number of labels will be
 * subject to rollover, and thus unexpected behavior.
 * */
char *new_label()
{
    char *label_ = malloc(32);
    label_[0] = 'L';
    label_[1] = 0;
    char *number = malloc(30);
    sprintf(number, "%d", label_num);
    strcat(label_, number);
    if (number)
        free(number);
    label_num++;
    return label_;
}

/*
 * Globals get declared in the data section, and are referenced with
 * an offset to the global pointer.
 */
void handle_globals(tree *node, FILE *filep)
{
    tree *curr = node->child->child;
    if ( curr->nodeKind == FUNDECL)
        return;
    while (curr->next != NULL && curr->nodeKind != FUNDECL)
    {
        fprintf(filep, "%s:\t.word\t0\n", curr->id->string);
        if (print_std)
            fprintf(stdout, "%s:\t.word\t0\n", curr->id->string);
        curr = curr->next;
    }
}

int is_global(tree *node)
{
    if ( node->id->scope == global_var_scope )
        return 1;
    return 0;
}

/*
 * Due to our unorthodox register allocation scheme, we place the first
 * 7 local variables into "t" registers, and any remaining local variables
 * get placed on the stack, only popped when needed and then immediately pushed back on.
 */
void handle_locals(tree *node, FILE *filep)
{
    // make sure we actually have locals to store
    if (!node->child)
        return;
    // Push the first 7 into the "t" registers.
    tree *curr = node->child;
    int i = 0;
    int regs[7] = {t0, t1, t2, t3, t4, t5, t6};
    while (curr && i < 7)
    {
        curr->id->allocated = 1;
        curr->id->reg = regs[i];
        if (debug)
            fprintf(stdout, "Assigned %s to register %d\n", curr->id->string, curr->id->reg);
        registers[ regs[i] ].type = 1; /* type "variable" */
        registers[ regs[i] ].occupied = 1; /* occupied */
        registers[ regs[i] ].contents = curr->id; /* pointer to variable struct */
        curr = curr->next;
        i++;
    }
    // See how many locals we have left
    i = 0;
    tree *overflow = curr;
    while (curr)
    {
        i += 1;
        curr = curr->next;
    }
    if (debug)
        fprintf(stdout, "Found %d excess local variables\n", i);
    // Set the offset from the stack pointer for all remaining local variables
    for (i; i > 0; i--)
    {
        if (debug)
            fprintf(stdout, "set offset for variable %d\n", i);
        overflow->id->offset = i * 4 + 48;
        overflow = overflow->next;
    }


}

/*
 * When a function call is encountered, we first save all "s" registers
 * by pushing them onto the stack. This allows us to then save all the "t"
 * registers that hold local variables into the "s" registers we just vacated.
 * This is in accordance to our stated calling convention. Next we save our sole
 * parameter onto the stack (regardless of whether we actually had one given to us).
 * Finally, if we're sending an argument, we place it into $a0, before generating
 * our "jal" instruction.
 *
 * Upon return, we reverse the same process we undertook at the beginning.
 *
 * This process is of course, wildly inefficient, but ultimately creates a correct result.
 */
int func_call(tree *node)
{
    asm_node *line;
    int tregs[7] = {t0, t1, t2, t3, t4, t5, t6};
    int sregs[7] = {s0, s1, s2, s3, s4, s5, s6};
    int i;
    // push the current s registers onto the stack, in their designated place
    for (i = 0; i < 7; i++)
    {
        generate_asm_node("sw", sregs[i], sp, 0, 16 + i * 4, NULL, load);
    }
    // Save our temp registers into s registers
    for (i = 0; i < 7; i++)
    {
        // copy temps into s registers
        generate_asm_node("move", 0, sregs[i], tregs[i], 0, NULL, move);
    }
    // Save our parameter (if we had one)
    generate_asm_node("sw", a0, sp, 0, 0, NULL, load);
    // Push our argument to a0
    if (node->child)
    {
        int result = expr(node->child->child);
        generate_asm_node("move", 0, a0, result, 0, NULL, move);
        free_reg(result);
    }
    // generate our jump and link
    generate_asm_node("jal", 0, 0, 0, 0, node->id->string, jumpLink);
    // Restore our argument (if we had one)
    generate_asm_node("lw", a0, sp, 0, 0, NULL, load);
    // Restore our temp registers
    for (i = 0; i < 7; i++)
    {
        // copy temps into s registers
        generate_asm_node("move", 0, tregs[i], sregs[i], 0, NULL, move);
    }
    // Restore previous s registers
    for (i = 0; i < 7; i++)
    {
        generate_asm_node("lw", sregs[i], sp, 0, 16 + i * 4, NULL, load);
    }
    return 2;
}

/* As per assignment instructions, generates assembly code
 * to output a single integer argument to the function "output"
 *
 */
void output(tree *node, FILE *filep)
{
    asm_node *line;
    // put the desired output into a0
    int result = expr(node->child->child);
    generate_asm_node("move", 0, a0, result, 0, NULL, move);
    // load 1 into v0
    generate_asm_node("addi", v0, 0, 0, 1, NULL, immediate);
    // write syscall
    generate_asm_node("syscall", 0, 0, 0, 0, NULL, syscall);
}

/* 
 * Handle return statements.
 */
void retrn(tree *node)
{
    int rtn_val = expr(node->child);
    // move return value to v0
    generate_asm_node("move", 0, v0, rtn_val, 0, NULL, move);
    generate_asm_node("lw", ra, sp, 0, 44, NULL, load);
    // adjust stack
    generate_asm_node("addi", sp, sp, 0, 48, NULL, immediate);
    // jump back
    generate_asm_node("jr", ra, 0, 0, 0, NULL, jump);
    free_reg(rtn_val);
}

/* Generates and returns a count of local variables for 
 * stack frame generation purposes.
 */
int count_locals(tree *node)
{
    fprintf(stdout, "Inside count_locals with %d\n", node->nodeKind);
    fflush(stdout);
    if (!node->child)
        return 0;
    tree *curr = node->child;
    int i = 0;
    while (curr && i < 7)
    {
        curr = curr->next;
        i++;
    }
    if (debug)
        fprintf(stdout, "%d local variables\n", i);
    i = 0;
    tree *overflow = curr;
    while (curr)
    {
        i += 1;
        curr = curr->next;
    }
    if (debug)
        fprintf(stdout, "%d extra local variables\n", i);
    return i;
}

void generate_asm_node(char *inst, int reg1, int reg2, int reg3, int immed, char *offset, int type)
{
    asm_node *line = malloc(sizeof(asm_node));
    line->inst = inst;
    line->reg1 = reg1;
    line->reg2 = reg2;
    line->reg3 = reg3;
    line->immed = immed;
    line->offset = offset;
    line->type = type;
    add_asm_node(line);
}

