#include <stdio.h>
#include <getopt.h>
#include "hash.h"
#include "tree.h"
#include "codegen.h"


unsigned int errors = 0;        /* number of errors encountered */
int ret = 0;                    /* Return value of the driver */
tree *parseTree = NULL;         /* The root of the AST */
hash_table_t *current_scope;    /* The currently active scope */
hash_table_t *global_var_scope; /* "root", or global variable table */
hash_table_t *global_func_scope;/* "root", or global function table */
int debug = 0;                  /* debug messages on or off */
int print_std = 0;              /* print code to stdout as well as a file */
int yyparse(void);

int main (int argc, char *argv[])
{
    int dump_tables = 0, print_ast = 0;
    int size_of_table = 128;
    global_var_scope = create_symbol_table(size_of_table);
    global_func_scope = create_symbol_table(size_of_table);
    current_scope = global_var_scope;

    char c;
    while((c = getopt(argc, argv, "stdo")) != -1)
    {
        switch (c)
        {
            case 's':
                dump_tables = 1;
                break;
            case 't':
                print_ast = 1;
                break;
            case 'd':
                debug = 1;
                break;
            case 'o':
                print_std = 1;
                break;
        }
    }
    yyparse();
    if (errors == 0)
    {
        puts ("accepted");
        if (dump_tables)
        {
            hash_table_t *curr;
            dump_hash_table(global_func_scope);
            dump_hash_table(global_var_scope);
            if (global_var_scope->child != NULL)
            {
                curr = global_var_scope->child;
                while (curr != NULL)
                {
                    dump_hash_table(curr);
                    curr = curr->next;
                }
            }
        }
        if (parseTree)
        {
            if (print_ast)
                printAst (parseTree, 0);
            codeGen(parseTree);
        }
    }
    else
    {
        printf ("rejected with %d error(s)\n", errors);
        ret = 1;
    }
    /* free the variable scope tables */
    hash_table_t *curr;
    hash_table_t *temp;
    if (global_var_scope->child != NULL)
    {
        curr = global_var_scope->child;
        while (curr != NULL)
        {
            temp = curr->next;
            free(curr);
            curr = temp;
        }
    }
    free_table(global_var_scope);
    /* free the global func table */
    free_table(global_func_scope);
    return ret;
}
