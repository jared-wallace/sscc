#ifndef TREE_H
#define TREE_H

#include <stdio.h>
#include <stdlib.h>
#include "hash.h"

/* tree node structure */
typedef struct treenode tree;
typedef struct _list_t_ list_t;
struct treenode
{
    int nodeKind;
    int val;        /* integer value, or type, depending */
    char *str_val;  /* unhashed string value */
    int type;       /* type of value stored, char/int/string */
    struct _hash_table_t_ *scope_table; /* pointer to scope where identifier is located */
    list_t *id;     /* the structure of the element in the symbol table */
    int arr_size;   /* The declared size of the array */
    int arr;        /* 1 for has size/index, 2 for no size specified */
    tree *parent;   /* just what it sounds like */
    tree *next;     /* next sibling in same level */
    tree *child;    /* first child */
};

/* constructors */
tree *makeTree (int kind);
tree *makeTreeWithIntVal (int kind, int val);
tree *makeTreeWithIDVal (int kind, char *val);

/* copy constructor */
tree *copyTree(int kind, tree *target);

/* populators */
void addChild (tree *parent, tree *child);
void printAst (tree *root, int nestLevel);

/* tree manipulation macros */
#define getFirstChild(node) (node->child)
#define getNextSibling(node) (node->next)

/* Valid kinds of nodes. This list must match with nodeNames[] in tree.c */
enum nodeTypes
{
    PROGRAM,
    DECLLIST,
    DECL,
    VARDECL,
    TYPESPECIFIER,
    FUNDECL,
    FORMALDECLLIST,
    FORMALDECL,
    FUNBODY,
    LOCALDECLLIST,
    STATEMENTLIST,
    STATEMENT,
    COMPOUNDSTMT,
    ASSIGNSTMT,
    CONDSTMT,
    LOOPSTMT,
    RETURNSTMT,
    VAR,
    EXPRESSION,
    RELOP,
    ADDEXPR,
    ADDOP,
    TERM,
    MULOP,
    FACTOR,
    FUNCCALLEXPR,
    ARGLIST,
    OUTPUT
};
enum { TYPE_ARRAY = 400 };
#endif
