DEBUG ?= 1
ifeq ($(DEBUG), 1)
	CFLAGS=-g3 -DDEBUG -I include
else
	CFLAGS=-DNDEBUG -I include
endif

CC = gcc $(CFLAGS)
YFLAGS= -dvt -o src/$*.tab.c
LFLAGS= --outfile=src/$*.yy.c
LEX=lex $(LFLAGS)
YACC=bison $(YFLAGS)
OBJECTS= parser.tab.o scanner.yy.o hash.o tree.o driver.o codegen.o
EXECUTABLE=mcc
LIBRARIES= -ll

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LIBRARIES)

%.yy.o: src/%.l
	$(LEX) $<
	$(CC) -c src/$*.yy.c

%.tab.o: src/%.y
	$(YACC) $<
	$(CC) -c src/$*.tab.c

%.o: src/%.c
	$(CC) -c $<

test:
	tests/run_tests.py
clean:
	rm -f $(OBJECTS) $(EXECUTABLE) src/parser.output src/parser.tab.c src/parser.tab.h src/scanner.yy.c index.html
